After you have a brand new box with Winodws 7/8/10 installed, the first thing you do is install Chocolatey
Chocolatey is like apt-get for Windows.

In Cmd.exe run

```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

More info on installation: https://chocolatey.org/install

After that run choco install cleaninstall.config  --confirm

To install the basic software needed for a developer at Mindbus